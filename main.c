#include "atmel_start_pins.h"
#include <atmel_start.h>
#include <stdio.h>
#include <stdint.h>

#include "driver_examples.h"

/**
 * Example of using SPI_MASTER to write "Hello World" using the IO abstraction.
 */

struct io_descriptor *uart_dbg, *spi_master;

int _write(int fd, const void *buf, size_t count) { io_write(uart_dbg, buf, count); return count; }

int _read(int fd, const void *buf, size_t count){ return -1;};

static void err_cb_UART_DBG(const struct usart_async_descriptor *const io_descr) {
  asm("nop");
}

int main(void) {
  /* Initializes MCU, drivers and middleware */
  atmel_start_init();

  usart_async_get_io_descriptor(&UART_DBG, &uart_dbg);
  usart_async_register_callback(&UART_DBG, USART_ASYNC_ERROR_CB, err_cb_UART_DBG);

  usart_async_enable(&UART_DBG);

  spi_m_sync_get_io_descriptor(&SPI_MASTER, &spi_master);

	spi_m_sync_enable(&SPI_MASTER);

  /* Replace with your application code */
    dac60508_check_device_id();
  while (1) {
  }
}

int dac60508_check_device_id() {
  uint8_t check_device_id_cmd[] = {0x81, 0x00, 0x00};
  uint8_t check_device_id_ans[3];
  static uint8_t example_SPI_MASTER[12] = "Hello World!";

  gpio_set_pin_level(SPI_EN, 0);
  io_write(spi_master, check_device_id_cmd, sizeof(check_device_id_cmd));
  gpio_set_pin_level(SPI_EN, 1);
  gpio_set_pin_level(SPI_EN, 0);
  io_read(spi_master, check_device_id_ans, sizeof(check_device_id_ans));
  for(size_t i = 0; i < sizeof(check_device_id_ans); i++)
    printf("GOT : %02x\n", check_device_id_ans[i]);
  gpio_set_pin_level(SPI_EN, 1);

  return 0;

} 